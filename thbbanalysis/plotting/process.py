import glob
import os
import pickle
from thbbanalysis.common.tools import *
from thbbanalysis.common.branches import *
from thbbanalysis.histogramming.classes import *


def process_plots(cfg):
    plots = cfg.plots
    indir = cfg.settings['indir']
    for tree, plotobjs in plots.items():
        for obj in plotobjs:
            sample_hists = {}
            for sample in obj.samples:
                f = open(f"{indir}{sample}_histos.pkl", "rb")
                var_hists = pickle.load(f)[tree]
                sample_hists[sample] = [hist for var, hist in var_hists.items() if var == obj.var][0]
            
            outdir = cfg.settings['outdir']
            ext = cfg.settings['format']
            outpath = outdir+'/'+f'{obj.name}_{tree}.{ext}'
            obj.draw(outpath, sample_hists)

import mplhep
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib import gridspec
from matplotlib.artist import setp
from matplotlib.ticker import AutoMinorLocator
import matplotlib.ticker
import numpy as np
import boost_histogram as bh
import awkward as ak
import os
from thbbanalysis.common import logger

logger = logger.ColoredLogger()

class Legend(object):
    
    def __init__(self, **kwargs):
        self.labels = None
        self.show = True
        self.frameon = True
        self.ncol = 1
        self.columnspacing = 2.0
        self.fontsize = 30
        
        for attr, val in kwargs.items():
            setattr(self, attr, val)  
    
    def __iter__(self):
        for attr, value in self.__dict__.iteritems():
            yield attr, value
    
    def set_labels(self, labels, samples=None):
        if samples is not None:
            self.labels = [v for k, v in labels.items() if k in samples]
            if len(self.labels) != len(samples):
                logger.error("Provided a legned labels map, but some samples are not keys in it..")
        else:
            self.labels = labels
    
    def get_settings_dict(self):
        settings = {}
        for attr, val in vars(self).items():
            if attr in ['labels', 'show']:
                continue
            settings[attr] = val
        return settings
   
    # Legend Settings
    def draw_legend(self, ax):
        if self.show:            
            ax.mplax.legend(self.labels, loc='upper right',
                              borderaxespad=0.,
                              **self.get_settings_dict())

class Axis(object):
    def __init__(self, fig, subplot, **kwargs):
        self.fig = fig
        if 'sharex' in kwargs:
            self.mplax = self.fig.add_subplot(subplot, sharex=kwargs['sharex'])
        else:
            self.mplax = self.fig.add_subplot(subplot)

        # Things to set when we have objects
        self.colors = ['#e6194b', '#3cb44b', '#ffe119', 
                       '#4363d8', '#f58231', '#911eb4',
                       '#46f0f0', '#f032e6', '#bcf60c', 
                       '#fabebe', '#008080', '#e6beff', 
                       '#9a6324', '#fffac8', '#800000', 
                       '#aaffc3', '#808000', '#ffd8b1', 
                       '#000075', '#808080', '#ffffff', 
                       '#000000']
        # Legend settings
        self.legend_settings = {}
        self.labels = None
        if 'legend_settings' in kwargs:
            self.legend = Legend(**kwargs['legend_settings'])
        # Axes settings 

        #Y-Axis
        self.ytitle = ''
        self.ylabel = {}
        self.ylabel['fontsize'] = 24
        self.yticklabel = {}
        self.yticklabel['labelsize'] = 24        
        self.yaxis = {}
        self.yaxis['scale'] = "linear"
        self.yaxis['ylim'] = None
        # for Ratio
        self.low_ytitle = ''
        self.low_ylabel = {}
        self.low_ylabel['fontsize'] = 24
        self.low_yticklabel = {}
        self.low_yticklabel['labelsize'] = 24        
        self.low_yaxis = {}
        self.low_yaxis['scale'] = "linear"
        self.low_yaxis['ylim'] = None
        # X-Axis
        self.xlabel = {}
        self.xlabel['fontsize'] = 24
        self.xticklabel = {}
        self.xticklabel['labelsize'] = 24
        self.xaxis = {}
        self.xaxis['scale'] = "linear"
        self.xaxis['xlim'] = None
        self.xtitle = ''

        # Text settings 
        self.text = {}
        self.text['text'] = []
        self.text['x'] = 0.05
        self.text['y'] = 0.8
        self.text['vshift'] = 0.05
        self.text['hshift'] = 0.05
        self.text['fontdict'] = {'size': 30, 'ha': 'left', 'va': 'center'}
        self.text['hshift'] = 0.05
        #self.presel_text = self.text
        # Style Settings
        self.style = 'ATLAS'
        self.exp_label = {}
        self.exp_label['text']= 'Work in Progress'
        self.exp_label['lumi']= 139
        self.exp_label['year'] = None
        self.title = ''
        self.stacked = False
        self.norm = False
        self.norm_binwidth = None
        
        for attr, val in kwargs.items():
            if isinstance(val, dict) and isinstance(getattr(self, attr), dict):
                for inner_attr, inner_val in val.items():
                    attrobj = getattr(self, attr)
                    attrobj[inner_attr] = inner_val
            else:
                setattr(self, attr, val)

    #=======================================================
    #=================== General Style ====================
    #=======================================================
    def set_atlas_text(self):
        # ATLAS Settings
        mplhep.style.use(self.style)
        if self.style == 'ATLAS':
            mplhep.atlas.label(self.exp_label['text'], lumi = self.exp_label['lumi'], year = self.exp_label['year'], ax=self.mplax)        
    
    def set_ylim(self, low = False):
        if self.yaxis['ylim'] is not None and not low:
            self.mplax.set_ylim(*self.yaxis['ylim'])
        if  self.low_yaxis['ylim'] is not None and low:
            self.mplax.set_ylim(*self.low_yaxis['ylim'])

    def set_xlim(self):
        if self.xaxis['xlim'] is not None:
            self.mplax.set_xlim(*self.xaxis['xlim'])
    
    def set_title(self):
        self.mplax.set_title(self.title)
    
    def set_grid(self):
        self.mplax.grid(True, which='major', alpha=0.40)
    
    def set_style(self):
        self.set_atlas_text()
        self.set_ylim()
        self.set_xlim()
        self.set_title()
        self.set_grid()
        plt.tight_layout()
    
    #=======================================================
    #=================== Tick settings ====================
    #=======================================================

    def set_ticks(self):
        self.mplax.xaxis.set_minor_locator(AutoMinorLocator(5))
        self.mplax.yaxis.set_minor_locator(AutoMinorLocator(5))
        self.mplax.tick_params(which='minor', length=6)
        self.mplax.tick_params(which='major', length=12)
    
        ticks_axes = {x: True for x in ['bottom', 'top', 'left', 'right']}
        self.mplax.tick_params(axis='x', direction='in', which='both', **self.xticklabel, **ticks_axes)
        self.mplax.tick_params(axis='y', direction='in', which='both', **self.yticklabel, **ticks_axes)
        #self.mplax.ticklabel_format(axis="both", style="sci", scilimits=(0, 0))
        self.mplax.yaxis.offsetText.set_fontsize(18)
        self.mplax.xaxis.offsetText.set_fontsize(18)
    
    #=======================================================
    #===================  Axis label settings ====================
    #=======================================================

    def set_xlabel(self, low=False):
        self.mplax.set_xlabel(self.xtitle, **self.xlabel)
    
    def set_ylabel(self, low=False):
        if not low:
            self.mplax.set_ylabel(self.ytitle, ha="center", **self.ylabel)
        else:
            self.mplax.set_ylabel(self.low_ytitle, ha="center", **self.low_ylabel)
    
    def set_axis_labels(self):
        self.set_xlabel()
        self.set_ylabel()
    #=======================================================
    #===================  Text settings ====================
    #=======================================================
    def set_text(self):
        # x = self.presel_text['x']
        # y = self.presel_text['y']        
        # if self.presel_text != self.text:
        #     self.mplax.text(x =x , y=y, s= self.presel_text['text'], transform= self.mplax.transAxes, **self.presel_text['fontdict'])
        # x+=self.presel_text['hshift']
        # y-=self.presel_text['vshift']
        for txt in self.text['text']:
            x = self.text['x']
            y = self.text['y']
            if txt == self.text['text'][0]:
                self.mplax.text(x, y, txt, transform= self.mplax.transAxes, **self.text['fontdict'])
            else:
                x+=self.text['hshift']
                y-=self.text['vshift']
                self.mplax.text(x, y, txt, transform= self.mplax.transAxes, **self.text['fontdict'])
    
class Canvas(object):
    default_name = 'Canvas.pdf'
    def __init__(self, name, **kwargs):
        
        self.name = name
        self.can_width = 15
        self.can_height = 10
        self.outpath = None
        
        for attr, val in kwargs.items():
            setattr(self, attr, val)
    
        self.fig = Figure((self.can_width, self.can_height))
        self.canvas = FigureCanvas(self.fig)
        gs = gridspec.GridSpec(1, 1)
        self.ax = Axis(self.fig, gs[0], **kwargs)
        self.fig.set_tight_layout(True)


    def save(self, outpath=None):
        output = outpath if outpath is not None else self.outpath
        assert output, "An output file name is required"

        out_dir, out_file = os.path.split(output)
        if out_dir and not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        self.canvas.print_figure(output, bbox_inches='tight')

    def __enter__(self):
        if not self.outpath:
            self.outpath = self.default_name
        return self

    def __exit__(self, extype, exval, extb):
        if extype:
            return None
        self.save(outpath=self.outpath)
        return True

    def normalise(self, hists):
        
        hist_vals = []
        hist_errs = []
        norm = self.ax.norm.upper()
        for hist in hists:
            if norm == "AREA":
                # Normalise such that sum of bin areas = 1
                rel_err = divide(np.sqrt(hist.variances()),hist.values())
                vals = hist.values()/np.diff(hist.axes[0].edges)/hist.values().sum()
                errs = rel_err*vals
                hist_vals.append(vals)
                hist_errs.append(errs)
            elif norm == "HEIGHT":
                # Normalise such that sum of bin heights = 1
                rel_err = divide(np.sqrt(hist.variances()),hist.values())
                vals = hist.values()/hist.values().sum()
                errs = rel_err*vals
                hist_vals.append(vals)
                hist_errs.append(errs)
            else:
                hist_vals.append(hist.values())
                hist_errs.append(np.sqrt(hist.variances())) 
        return hist_vals, hist_errs

class Hist1DPlot(Canvas):
    def __init__(self, var, samples, *args, **kwargs):
        self.var = var
        self.samples = samples if isinstance(samples,list) else [samples]
        super(Hist1DPlot, self).__init__(*args, **kwargs)
    
    def __buildLayout__(self):
        self.ax.set_style()
        self.ax.set_ticks()
        self.ax.set_axis_labels()
        self.ax.set_text()

    def __buildLegend__(self, samples):
        leg = self.ax.legend
        leg.set_labels(self.ax.labels, samples = samples)
        leg.draw_legend(self.ax)
        mplhep.plot.yscale_legend(ax=self.ax.mplax)
        
    def draw(self, outpath, samples_to_hists):
        self.outpath = outpath
        hists = [hist for sample, hist in samples_to_hists.items()]
        samples = [sample for sample, hist in samples_to_hists.items()]
        with self as can:
            hists_vals, hist_errs = self.normalise(hists)
            hist_type = 'fill' if can.ax.stacked else 'step'
            if self.ax.norm and not can.ax.norm_binwidth:
                mplhep.histplot(hists_vals, bins = hists[0].axes[0].edges, ax=can.ax.mplax, histtype = hist_type, 
                                yerr=hist_errs, stack=can.ax.stacked, color=can.ax.colors[:len(hists)])
            elif not self.ax.norm and can.ax.norm_binwidth:
                
                mplhep.histplot(hists, ax=can.ax.mplax, histtype = hist_type, yerr=True, stack=can.ax.stacked, 
                                binwnorm=can.ax.norm_binwidth, color=can.ax.colors[:len(hists)])
            elif not self.ax.norm and not can.ax.norm_binwidth:
                mplhep.histplot(hists, ax=can.ax.mplax, histtype = hist_type, yerr=True, stack=can.ax.stacked, 
                                color=can.ax.colors[:len(hists)])
     
            can.__buildLayout__()
            samples = samples if isinstance(can.ax.labels, dict) else None
            can.__buildLegend__(samples)


class CanvasWithRatio(object):
    def __init__(self, name, **kwargs):
        #super(CanvasWithRatio, self).__init__(name, **kwargs)
        self.name = name
        self.fig = Figure((15,10))
        self.canvas = FigureCanvas(self.fig)
        gs = gridspec.GridSpec(2, 1, height_ratios=[5, 1])
        self.ax = Axis(self.fig, subplot=gs[0], **kwargs)
        self.ax2 = Axis(self.fig, subplot=gs[1], sharex=self.ax.mplax,**kwargs)
        self.fig.subplots_adjust(hspace=0.1)  
        setp(self.ax.mplax.get_xticklabels(), visible=False)
    
    def save(self, outpath=None):
        output = outpath if outpath is not None else self.outpath
        assert output, "An output file name is required"

        out_dir, out_file = os.path.split(output)
        if out_dir and not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        self.canvas.print_figure(output, bbox_inches='tight')

    def __enter__(self):
        if not self.outpath:
            self.outpath = self.default_name
        return self

    def __exit__(self, extype, exval, extb):
        if extype:
            return None
        self.save(outpath=self.outpath)
        return True
    
    def normalise(self, hists):
        
        hist_vals = []
        hist_errs = []
        norm = self.ax.norm.upper()
        for hist in hists:
            if norm == "AREA":
                # Normalise such that sum of bin areas = 1
                rel_err = divide(np.sqrt(hist.variances()),hist.values())
                vals = hist.values()/np.diff(hist.axes[0].edges)/hist.values().sum()
                errs = rel_err*vals
                hist_vals.append(vals)
                hist_errs.append(errs)
            elif norm == "HEIGHT":
                # Normalise such that sum of bin heights = 1
                rel_err = divide(np.sqrt(hist.variances()),hist.values())
                vals = hist.values()/hist.values().sum()
                errs = rel_err*vals
                hist_vals.append(vals)
                hist_errs.append(errs)
            else:
                hist_vals.append(hist.values())
                hist_errs.append(np.sqrt(hist.variances())) 
        return hist_vals, hist_errs

class Hist1DRatioPlot(CanvasWithRatio):
    def __init__(self, var, samples, ref_sample, *args, **kwargs):
        super(Hist1DRatioPlot, self).__init__(*args, **kwargs)
        self.var = var
        self.samples = samples if isinstance(samples,list) else [samples]
        self.refsample = ref_sample
    
    def __buildLayout__(self):
        # Main Plot
        
        self.ax.set_style()
        self.ax.set_ticks()
        self.ax.set_ylabel()
        # Ratio Panel
        self.ax2.set_ticks()
        self.ax2.set_xlabel()
        self.ax2.set_ylabel(low=True)
        self.ax2.set_xlim()
        self.ax2.set_ylim(low=True)


    def __buildLegend__(self, samples):
        leg = self.ax.legend
        leg.set_labels(self.ax.labels, samples = samples)
        leg.draw_legend(self.ax)
        mplhep.plot.yscale_legend(ax=self.ax.mplax)
        
    
    def draw(self, outpath, samples_to_hists):
        self.outpath = outpath
        hists = [hist for sample, hist in samples_to_hists.items()]
        samples = [sample for sample, hist in samples_to_hists.items()]
        with self as can:
            hists_vals, hist_errs = self.normalise(hists)
            hist_type = 'fill' if can.ax.stacked else 'step'

            if self.ax.norm and not can.ax.norm_binwidth:
                mplhep.histplot(hists_vals, bins = hists[0].axes[0].edges, ax=can.ax.mplax, histtype = hist_type, 
                                yerr=hist_errs, stack=can.ax.stacked, color=can.ax.colors[:len(hists)])
            elif not self.ax.norm and can.ax.norm_binwidth:
                mplhep.histplot(hists, ax=can.ax.mplax, histtype = hist_type, yerr=True, stack=can.ax.stacked, 
                                binwnorm=can.ax.norm_binwidth, color=can.ax.colors[:len(hists)])
            elif not self.ax.norm and not can.ax.norm_binwidth:
                mplhep.histplot(hists, ax=can.ax.mplax, histtype = hist_type, yerr=True, stack=can.ax.stacked, 
                                color=can.ax.colors[:len(hists)])
            
            can.__buildLayout__()
            samples = samples if isinstance(can.ax.labels, dict) else None
            can.__buildLegend__(samples)

            ref_hist = samples_to_hists[self.refsample]
            ratios = [ratio_from_hists(hist, ref_hist) for hist in hists if hist!=ref_hist]
            ratio_colors = [can.ax.colors[i] for i in range(len(hists)) if hists[i] != ref_hist]
            ratios_errs = [ratio_uncert_from_hists(hist, ref_hist) for hist in hists if hist!=ref_hist]
            mplhep.histplot(ratios, ref_hist.axes[0].edges, yerr = ratios_errs, 
                            ax = can.ax2.mplax, histtype='errorbar',
                            color = ratio_colors)


def divide(list1,list2):
    list1 = np.array(list1)
    list2= np.array(list2)
    if list1.shape != list2.shape:
        logger.error("Trying to divide 2 arrays of different shapes")
    if any(elem ==0 for elem in list2):
        return np.array([list1[i]/elem2 if elem2!=0 else 0 for i, elem2 in enumerate(list2)])
    else:
        return np.divide(list1, list2)

def ratio_from_hists(num_h, denom_h):
    return divide(num_h.values(), denom_h.values())

def ratio_uncert_from_hists(num_h, denom_h):

    num_err, denom_err = np.sqrt(num_h.variances()), np.sqrt(denom_h.variances())
    ratio = ratio_from_hists(num_h, denom_h)

    num_term = divide(num_err, num_h.values())**2
    denom_term = divide(denom_err, denom_h.values())**2

    return ratio*np.sqrt(num_term + denom_term)


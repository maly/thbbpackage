from pydoc import importfile
from thbbanalysis.plotting.classes import *
from thbbanalysis.common import logger
from thbbanalysis.common.branches import * 
from thbbanalysis.common.selection import * 
import os 
import types

#logger = logger.ColoredLogger()

class PlotConfig(object):
    
    def __init__(self, path):
        self.module = importfile(str(path))
        self.plots = None
        self.settings = {'indir': None, 'outdir': './', 'format': 'pdf'}
        self.required_settings = ['indir']
        self.supported_settings = {'format': ['pdf', 'png']}
        self.supported_plots = (Hist1DPlot, Hist1DRatioPlot)
    
    def load(self):
        self.validate_plots()
        self.plots = self.module.plots
        # General Settings
        settings_valid = self.validate_settings()
        
        if settings_valid:
            given_settings = self.module.general_settings
            given_settings = {k.lower(): v for k, v in given_settings.items()}
            for k, v in given_settings.items():
                self.settings[k] = v 
            if any(os.path.isdir(indir) for indir in self.settings['indir']):
                pass
            else:
                logger.error(f'Provided input directory is not found. Please check path')
            
            # if isinstance(self.settings['indir'], str):
            #     self.settings['indir'] = [self.settings['indir']]
            
            self.check_supported_settings()
        
    def validate_plots(self):
        bad_plots_msg = "A map called plots must be provided with the form {tree: [plot1, plot2,...]}"
        supported_plots = self.supported_plots
        try:
            plots = self.module.plots
        except AttributeError:
            logger.error(bad_plots_msg)
        if not isinstance(plots, dict):
            logger.error(bad_plots_msg)
        if not all(isinstance(plotobj, supported_plots) for tree, plotobjs in plots.items() for plotobj in plotobjs):
            logger.error(f"Only following plots are supported: {supported_plots}")
        
        return True
        
    def validate_settings(self):
        req_settings = self.required_settings
        bad_settings_msg = f"A general_settings map must be defined with at least the following keys {req_settings}"
        try:
            settings = self.module.general_settings
        except:
            logger.error(bad_settings_msg)
        
        settings = {k.lower(): v for k, v in settings.items()}
        if not all(given_setting in self.settings for given_setting in settings):
            for given_setting in self.settings:
                if given_setting not in self.settings:
                    logger.error(f'Unsupported setting {given_setting} provided. Acceptable settings are {list(self.settings)} ')
                
        return True

        
    def check_supported_settings(self):
        for k, v in self.settings.items():
            if k in self.supported_settings:
                if v not in self.supported_settings[k]:
                    logger.error(f"Unsupported value {v} for setting {k}. Supported values are {self.supported_settings[k]}")

        

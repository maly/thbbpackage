#=========== Pythonic Imports
import boost_histogram as bh
import numpy as np
import awkward as ak


class VarBinning(object):
    def __init__(self, binning):
        self.binning = binning
    
    def set_axis(self, ax_name):
        self.axis = ax_name

class RegBinning(VarBinning):
    def __init__(self, minimum, maximum, numbins):
        self.min = minimum
        self.max = maximum
        self.nbins = numbins
        self.axis = None
        super(RegBinning, self).__init__(np.linspace(self.min, self.max,self.nbins))


class Hist(object):
    '''
    Parent Histogram object, holds attributes common for Hist1D, Hist2D, etc.. 
    '''
    def __init__(self, var, name, binning, weights=1,  sel = None, regions = []):
        self.var = var if isinstance(var, list) else [var] # To support N-dim Histos
        self.name = name
        self.binning = binning if isinstance(binning, list) else [binning] # To support N-dim Histos
        # Assign axis names to binning 
        if isinstance(self.binning, list):
            axes = ['X','Y','Z'][:len(self.binning)]
            for i, ax_binning in enumerate(self.binning):
                ax_binning.set_axis(axes[i])
        self.sel = sel
        self.regions = regions
        self.weights = weights
        self.dim = None
        self.valid_dims()
    
    def valid_dims(self):
        return (len(self.var) == self.dim) and (len(self.binning) == self.dim)



class Hist1D(Hist):
    '''
    Child class that inherits from Hist class, but with self.dim = 1 
    '''
    def __init__(self, *args, **kwargs):
        super(Hist1D, self).__init__(*args, **kwargs)
        self.var = self.var[0]
        self.binning = [b for b in self.binning if b.axis == 'X'][0]
        self.axes = self.get_axes()
        self.dim = 1
    
    def get_axes(self):
        binning = self.binning
        if isinstance(binning, RegBinning):
            axis = bh.axis.Regular(binning.nbins, binning.min, binning.max)
        else:
            axis = bh.axis.Variable(binning.binning)
        return (axis, )

        
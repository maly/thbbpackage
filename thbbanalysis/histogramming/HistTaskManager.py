import dask
import boost_histogram as bh
from thbbanalysis.common.tools import *
from thbbanalysis.histogramming.DataHandler import DataHandler, get_req_branches
from thbbanalysis.common.logger import *

use_dask = None
class HistTaskManager(object):
    
    def __init__(self, hist_objs, new_branches, params):
        self._objs = hist_objs
        self._new_branches = new_branches
        self._files = params["files"]
        self._batch = params["batch"]
        self._reader = params["read_by"]
        self._dask = params["dask"]
        use_dask = self._dask
            
    def get_tasks(self):
        logger = ColoredLogger()
        results = []
        logger.info(f"Processing {len(self._files)} files in batches of size {self._batch}")
        ctr = 0
        obj_idx = {obj.name:[] for obj in self._objs}
        for i in range(0, len(self._files), self._batch):
            batch_results = []
            for j, file in enumerate(self._files[i:i+self._batch]):
                req_brs = get_req_branches(self._new_branches)
                req_brs += list(set([obj.var for obj in self._objs 
                                    if obj.var not in [nb["write_name"] for nb in self._new_branches]]))
                req_brs += list(set([arg for obj in self._objs  for arg in obj.sel.args 
                                    if arg not in [nb["write_name"] for nb in self._new_branches]]))        
    
                req_brs += list(set([obj.weights for obj in self._objs 
                                    if isinstance(obj.weights, str) and 
                                    obj.weights not in self._new_branches]))
                
                data = cond_dec(dask.delayed, self._dask)(self._reader)(file, req_brs)[0]
                data_handler = cond_dec(dask.delayed, self._dask)(DataHandler)(data, self._new_branches, **{"fname": f"{file}" } )
                
                for obj in self._objs:
                    var_data = data_handler.make_numpy(obj.var, obj.sel)
                    weights  = data_handler.make_numpy(obj.weights, obj.sel, mask_like = var_data)
                    var_data = data_handler.compress_data(var_data)
                    weights  = data_handler.compress_data(weights)
                    batch_results.append(self.make_and_fill(obj, var_data, weights))
                    obj_idx[obj.name].append(ctr)
                    ctr += 1
            results.append(batch_results)

        return results, obj_idx
    
    def combine_hists(self, results, idxs):

        dd = defaultdict(int)
        for i, obj in enumerate(self._objs):
            for result_idx in idxs[obj.name]:
                dd[obj.name] += results[result_idx]
        return dd
    
    def conditional_decorator(dec):
        def decorator(func):
            def _(self, *args, **kwargs):
                f = func
                if self._dask:
                    f = dec(f)
                return f(self, *args, **kwargs)
            return _
        return decorator
    
    @conditional_decorator(dask.delayed)
    def make_and_fill(self, obj, data, weights):
        axes = obj.axes
        h = bh.Histogram(*axes, storage=bh.storage.Weight())
        h.fill(data, weight = weights)

        return h
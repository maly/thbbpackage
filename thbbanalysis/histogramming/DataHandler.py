#=========== Pythonic Imports
import numpy as np
import awkward as ak
#============ thbbanalysis Imports
from thbbanalysis.common.logger import ColoredLogger
from thbbanalysis.common.branches import *

class DataHandler(object):   
    '''
    Class responsible for converting data types into numpy arrays ready for use by BoostHistograms
    '''
    def __init__(self, data, new_branches, **logging):
        
        logger =  ColoredLogger()
        logger.info(f"Processing {logging['fname']} ...")
        self._data = self.build_new_branches(data, new_branches)
        self._datatype = type(self._data)
        self._vardata = None
        self._vardata_type = None
       
    def get_var(self, var, sel):
        if type(var) in (int, float):   return var
        if sel is not None:
            var_data = self.apply_selection(sel, var)
            return var_data[var]
        else:
            return self._data[var]

    def apply_selection(self, sel, var):
        logger = ColoredLogger()
        '''
        Apply cuts to branches that have same shape as cut bool. Is that sensible?
        Needs Testing
        '''
        data = self._data
        args = sel.args
        args = [data[arg] if isinstance(arg,str) else arg for arg in args]
        cuts = sel.func
        return  {var: data[var][cuts(*args)]}

    def make_numpy(self, var, sel, mask_like = None):
        logger = ColoredLogger()

        self._vardata = self.get_var(var, sel)
        self._vardata_type = type(self._vardata)

        if self._vardata_type not in (np.ndarray, np.array, int, float):
            try:    
                self._vardata = self._vardata.to_numpy()
                self._vardata_type = type(self._vardata)
            except AttributeError as ae:
                logger.error(f"Casting array of type {self._vardata_type} to numpy is not supported")
            if mask_like is not None:
                self._vardata =  np.ma.masked_where(np.ma.getmask(mask_like), self._vardata)
                self._vardata_type = type(self._vardata)
            if self._vardata_type == np.ma.core.MaskedArray:
                return self._vardata

        return self._vardata
    
    def compress_data(self, data):
        if type(data) == np.ma.core.MaskedArray:    return data.compressed()
        else:   return data
    
    def build_new_branches(self, akdata: "ak.Array", new_branches,):
        logger = ColoredLogger()
        '''
        Method to create new branches from input, or from other new branches
        Args: 
            data: Awkward array with data from a tree in one input file (one sample chunk)
            new_branches: A map of new branches for one sample-tree combo 
        Return:
            data: Awkward array with data including newly calculated branches
        '''
        data = {}
        for field in akdata.fields: data[field] = akdata[field]

        new_branches_from_others: List["Branch"] = [] 
        for branch in new_branches:
            # if we need other new branches to compute this new branch, save it for later
            if len(set([br["write_name"] for br in new_branches]).intersection(set(branch["alg_args"])))!=0:
                new_branches_from_others.append(branch)
                continue
            try:
                # Get args and execute the new branch algo
                args = [data[arg] if branch["alg_arg_types"][i] == Branch else arg for i, arg in enumerate(branch["alg_args"])]
                data[branch["write_name"]] = branch["alg"](*args)
            except KeyError as missing_branch:
                for i, arg in enumerate(branch["alg_args"]):
                    if branch["alg_arg_types"][i] == Branch:
                        if arg not in data.fields:
                            logger.error("Branch {arg} not found in data or other new branches.")
        # If we have any branches that are saved for later proessing, 
        # re-run the method on them with the updated data containing new branches
        if(len(new_branches_from_others)!=0):
            data = self.build_new_branches(ak.Array(data), new_branches_from_others)
        return data

def get_req_branches(new_branches):
    req_branches = []
    for br in new_branches:
        args = br["alg_args"]
        req_branches.extend([arg for arg in args if all([nb["write_name"] !=arg for nb in new_branches])])
    return list(set(req_branches))
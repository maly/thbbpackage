#============== System and Python Imports
import glob, os
#=========== Pythonic Imports
import pickle
import dask
import time
#============ thbbanalysis Imports
from thbbanalysis.common.branches import Branch
from thbbanalysis.histogramming.HistTaskManager import HistTaskManager
from thbbanalysis.common.tools import *
from thbbanalysis.common.logger import ColoredLogger

logger = ColoredLogger()

FORMAT_TO_READ_METHOD = {
    'h5': h5py_to_ak,
    'json': json_to_ak,
    'parquet': parquet_to_ak,
}

FORMAT_TO_EXT = {
    'h5': '.h5',
    'json': '.json',
    'parquet': '.parquet',
}

def get_inputs(cfg: "HistConfig", sample: str, tree: str):
    '''
    Method to get a list of files to read in. This assumes all files
    are named sample_chunk*
    Args:
        cfg: Config object carrying all info provided by user in config file
        sample: Name of the sample being processed
        tree: Name of tree being processed
    Return
        files: A list of files to read
    '''
    indirs = cfg.settings['indir']
    ext = FORMAT_TO_EXT[cfg.settings['informat']]
    files = [infile for indir in indirs for infile in glob.glob(f"{indir}/{sample}_chunk*{tree}{ext}")]
    if len(files) == 0:
        logger.error(f"No files for sample {sample} were found in specified directories {indirs}")
    return files


def process_histos(cfg: "HistConfig"):
    '''
    High-level method called by user to start processing all histograms
    Args:
        cfg: Config object carrying all info provided by user in config file
    '''
    histograms = cfg.histograms
    informat = cfg.settings['informat']
    hists: Dict[str, Dict[str, Dict[str, "bh.Histogram"]]] = {}

    sample_tree_tasks = defaultdict(dict)
    sample_tree_results = defaultdict(dict)
    for sample, tree_to_hists in histograms.items():
        
        for tree, objs in tree_to_hists.items():

            regions = [obj.regions for obj in objs][0] # Just one object is enough (for now)
            pure_samp_name = sample
            for region in regions:
                if region in sample:    
                    pure_samp_name = sample.replace("_"+region, "")
                    break
            
            new_br_list = cfg.new_branches[sample][tree]
            new_brs = get_new_br_info(new_br_list)
            files = get_inputs(cfg, pure_samp_name, tree)
            params = dict(files = files, batch=4, read_by = FORMAT_TO_READ_METHOD[informat], dask = cfg.settings["dask"])
            # add metadata like extension to tools ...
            logger.info(f"Processing Sample:  {sample}")
            hm = HistTaskManager(objs, new_brs, params)

            tasks, idxs = hm.get_tasks()
            sample_tree_tasks[sample][tree] = (tasks, hm, idxs)

    
    t1 = time.time()
    for sample, tree_tasks in sample_tree_tasks.items():
        logger.info(f"Processing the following sample: {sample}")
        for tree, tasks_hm in tree_tasks.items():
            logger.info(f"Processing the following tree: {tree}")
            logger.info(f"Preparing new branches: {tree}")
            tasks, hm, idxs = tasks_hm
            results = []
            for task_batch in tasks:
                if cfg.settings["dask"]:    results.extend(dask.compute(*task_batch))
                else:   results.extend(task_batch)
            logger.info(f"!!Done!! computing objects for  Sample: {sample} & Tree: {tree}")
            name_to_obj = hm.combine_hists(results, idxs)
            sample_tree_results[sample][tree] = name_to_obj
            if cfg.settings["view"]:
                for name, obj in name_to_obj.items():
                    logger.info(f"Histogram: {name} - Sample: {sample} - Tree: {tree}")
                    print(obj)
    t2 = time.time()
    logger.info(f"Time Taken to compute all histograms is {t2-t1}s")
    

    hists = defaultdict(lambda: defaultdict(dict))
    for sample, tree_to_results in sample_tree_results.items():
        for tree, results in tree_to_results.items():
            for var, hist in results.items():
                hists[var][sample][tree] = hist
    

    logger.info("Saving all histograms...")
    save_objects(cfg, hists)


def save_objects(cfg, histograms):
    '''
    Method responsible for saving histograms in output file
    Args:
        cfg: Config object carrying all info provided by user in config file
        histograms: A nested map of the form {sample: {tree: {obj_name: obj}}}
    '''
    outdir = cfg.settings['outdir']
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    
    ext = cfg.settings['dumptoformat']
    for var in histograms:
        if ext == 'pkl':
            outpath = f"{outdir}{var}_histos.pkl"
            try:
                os.remove(outpath)
                logger.warning(f"Overwriting existing file {outpath}")
            except OSError:
                pass
            outfile = open(outpath, 'wb')
            pickle.dump(histograms[var], outfile, protocol=pickle.HIGHEST_PROTOCOL)
        elif ext == 'another':
            pass 

def create_new_branches(data: "ak.Array", new_branches):
    '''
    Method to create new branches from input, or from other new branches
    Args: 
        data: Awkward array with data from a tree in one input file (one sample chunk)
        new_branches: A map of new branches for one sample-tree combo 
    Return:
        data: Awkward array with data including newly calculated branches
    '''
    new_branches_from_others: List["Branch"] = [] 
    

    for branch in new_branches:
        # if we need other new branches to compute this new branch, save it for later
        if len(set([br["write_name"] for br in new_branches]).intersection(set(branch["alg_args"])))!=0:
            new_branches_from_others.append(branch)
            continue
        try:
            # Get args and execute the new beanch algo
            args = [data[arg] if branch["alg_arg_types"][i] == Branch else arg for i, arg in enumerate(branch["alg_args"])]
            data[branch["write_name"]] = branch["alg"](*args)
        except KeyError as missing_branch:
            for i, arg in enumerate(branch["alg_args"]):
                if branch["alg_arg_types"][i] == Branch:
                    if arg not in data.fields:
                        logger.error("Branch {arg} not found in data or other new branches.")
    # If we have any branches that are saved for later proessing, 
    # re-run the method on them with the updated data containing new branches
    if(len(new_branches_from_others)!=0):
        data = create_new_branches(data, new_branches_from_others)
    return data

def get_new_br_info(new_branches):
    return [{'write_name': br.write_name, 'alg_args': br.alg_args,
             'alg_arg_types': br.alg_arg_types, 'alg': func_deepcopy(br.alg)} 
              for br in new_branches]
    


#============== System and Python Imports
from pydoc import importfile
import os 
import types
#============ thbbanalysis Imports
from thbbanalysis.common import logger
from thbbanalysis.common.branches import * 
from thbbanalysis.common.selection import * 
from thbbanalysis.histogramming.Hist import *

class HistConfig(object):
    '''
    Class to handle configuration option from config module 
    '''
    def __init__(self, path):
        self.module = importfile(str(path))
        self.histograms = None
        self.new_branches = None
        self.settings = {'indir': None, 'outdir': './', 'dumptoformat': 'pkl', 'informat': 'h5', 'dask': False, 'view': False}
        self.supported_settings = {'dumptoformat': ['pkl'], 'informat': ['h5','json', 'parquet']}
        self.supported_objs = (Hist1D)
    

    def load(self):
        '''
        Method to set the attributes of the config class from input config module
        '''
        logger.info(f"Loading config file from {self.module.__name__}")
        #==== Validate Histograms in config and assign them if they pass =======
        self.validate_histos()
        self.histograms = self.module.histograms
        #==== Validate settings in config and assign them if they pass =======
        settings_valid = self.validate_settings()
        if settings_valid:
            given_settings = self.module.general_settings
            # Lower the case of all settings provided 
            given_settings = {k.lower(): v for k, v in given_settings.items()}
            # Assign settings to the settings attribute (dict)
            for k, v in given_settings.items():
                self.settings[k] = v 
            
            # Turn input directory into list, for consistency
            if isinstance(self.settings['indir'], str):
                self.settings['indir'] = [self.settings['indir']]
            # Check that chosen options are supported
            self.check_supported_settings()
        #==== Validate new_branches in config and assign them if they pass =======
        nb_valid = self.validate_new_branches()
        if nb_valid:
            self.new_branches = self.module.new_branches
        else: 
            # Make an empty set of new_branches for consistency 
            self.new_branches = {sample:{tree: []} for sample in self.histograms for tree in self.histograms[sample]}
        
        logger.info("Config file looks good!")
        
    def validate_histos(self):
        '''
        Make sure histograms map in config is valid 
        '''
        supported_objs = self.supported_objs
        bad_histos_msg = "A histograms map must be provided in the form {sample: {tree: [h1,..hn]}}"
        try:
            sample_to_tree_hist = self.module.histograms
        except AttributeError:
            logger.error(bad_histos_msg)
        lvl1_is_dict =  isinstance(sample_to_tree_hist, dict)
        lvl2_is_dict = all(isinstance(tree_to_hist, dict) for k, tree_to_hist in sample_to_tree_hist.items())
        lvl3_is_list = all(isinstance(hists, list) for sample, tree_to_hist in sample_to_tree_hist.items() for tree, hists in tree_to_hist.items())
        lvl4_are_objs = all(isinstance(h, supported_objs) for sample, tree_to_hist in sample_to_tree_hist.items() for tree, hists in tree_to_hist.items() for h in hists)
        valid_map = lvl1_is_dict*lvl2_is_dict*lvl3_is_list*lvl4_are_objs
        
        if not valid_map:
            if not lvl1_is_dict:
                logger.debug(f"The 'histograms' object provided is not a dict. You provided {type(tree_to_sample_to_hist)}")
                logger.error(bad_histos_msg)
            else:
                for sample, tree_to_hist in sample_to_tree_hist.items():
                    if not lvl2_is_dict:
                        if not isinstance(tree_to_hist, dict):
                            logger.debug(f"The object histograms[{sample}] provided is not a dict. You provided  {type(tree_to_hist)}")
                            logger.error(bad_histos_msg)
                    else:
                        for tree, hists in tree_to_hist.items():
                            if not lvl3_is_list:
                                if not isinstance(hists, list):
                                    logger.debug(f"The object histograms[{sample}][{tree}] provided is not a list. You provided  {type(hists)}")
                                    logger.error(bad_histos_msg)
                            else:
                                for hist in hists:
                                    if not lvl4_are_objs:
                                        logger.debug(f"The object histograms[{sample}][{tree}] provided is a list of non-recognised objects. First bad object has type {type(hist)}")
                                        logger.debug(f"Accepted objects ae {supported_objs}")
                                        logger.error(bad_histos_msg)
        
        # If map is valid in terms of types, check histograms
        samplenames = []
        for sample, tree_to_hist in sample_to_tree_hist.items():
            logger.debug(f"Parsing the following tree: {sample} ")
            samplenames.append(sample)
            treenames = []
            for tree, hists in tree_to_hist.items():
                logger.debug(f"Parsing the following sample: {tree} ")
                treenames.append(tree)
                histnames = []
                for hist in hists:
                    if isinstance(hist, Hist):
                        if not isinstance(hist.name, str):
                            logger.error(f"Object name is not a string. Please check. ")
                        histnames.append(hist.name)
                        if not isinstance(hist.var, (str,list)) or (isinstance(hist.var, list) and not all(isinstance(var, str) for var in hist.var)):
                            logger.error(f"Object {hist.name} var argument is not a string or list of strings. Please check. ")
                        if isinstance(hist.binning, list):
                            if not all(isinstance(binning, (VarBinning, RegBinning)) for binning in hist.binning):
                                logger.error(f"Object {hist.name} binning argument is not a recognsied type. Ensure (VarBinning or RegBinning) are used. ")
                        else:
                            if not isinstance(hist.binning, (VarBinning, RegBinning)) :
                                logger.error(f"Object {hist.name} binning argument is not a recognsied type. Ensure (VarBinning or RegBinning) are used. ")                            
                        if hist.weights is not None:
                            if not isinstance(hist.weights, (str, float, int)):
                                logger.error(f"Object {hist.name} weight argument is not a recognsied type. Please check. ")
                        if hist.sel is not None:
                            if not isinstance(hist.sel, Selection):
                                logger.error(f"Object {hist.name} sel argument is not a recognsied type. Please check it is a Selection object. ")
                            #Is it worth validating internal objects such as selection? 
                
                if len(set(histnames)) != len(histnames):
                    logger.error(f"All objects for a given tree-sample must have unique name arguments")
            if len(set(treenames)) != len(treenames):
                logger.error(f"All trees for a given sample must be unique")
        if len(set(samplenames)) != len(samplenames):
                logger.error(f"All samples must be unique")
        
        return True
        

    def validate_new_branches(self):
        '''
        Make sure new_branches map in config is valid
        '''
        bad_newbranches_msg = "A new_branches map must be provided in the form {sample: {tree: [Branch_1,..Branch_n]}}"
        try:
            new_branches = self.module.new_branches
        except:
            return False
        if not isinstance(new_branches, dict):
            logger.error(bad_newbranches_msg)
        if not all(isinstance(tree_to_branches, dict) for sample, tree_to_branches in new_branches.items()):
            logger.error(bad_newbranches_msg)
        if not all(isinstance(branches, list) for sample, tree_to_branches in new_branches.items() for tree, branches in tree_to_branches.items()):
            logger.error(bad_newbranches_msg)
        if not all(isinstance(br, Branch)  for sample, tree_to_branches in new_branches.items() for tree, branches in tree_to_branches.items() for br in branches):
            logger.error(bad_newbranches_msg)
        
        samplenames = []
        for sample, tree_to_branches in new_branches.items():
            samplenames.append(sample)
            treenames = []
            for tree, branches in tree_to_branches.items():
                treenames.append(tree)
                branchnames = []
                for branch in branches:
                    if not isinstance(branch.write_name, str): # Check on branch name 
                        logger.error(f"(Sample: {sample}, Tree: {tree}) - \n Branch name must be a string, you gave {type(branch.write_name)}")
                    branchnames.append(branch.write_name)
                    if not isinstance(branch.alg, (types.FunctionType, str)): # Check on branch algorithm 
                        logger.error(f"(Sample: {sample}, Tree: {tree}, Branch: {branch.write_name}) - \n \
                                        Branch 'alg' must either be a function or a string with branch name in input file")
                    if isinstance(branch.alg, types.FunctionType) and  branch.alg_args is None: # check branch algo gets arg if needed
                        logger.error(f"(Sample: {sample}, Tree: {tree}, Branch: {branch.write_name}) - \n \
                                        You provided a function to calculate new branch but not 'args'")
                    if isinstance(branch.alg, types.FunctionType) and  branch.alg_arg_types is None: # check branch algo gets arg types if needed
                        logger.error(f"(Sample: {sample}, Tree: {tree}, Branch: {branch.write_name}) - \n \
                                        You provided a function to calculate new branch but not 'args_types'")
                    if branch.alg_args is not None and branch.alg_arg_types is not None:
                        if len(branch.alg_args) != len(branch.alg_arg_types):
                            logger.error(f"Sample: {sample}, Tree: {tree}, Branch: {branch.write_name}) - \n \
                                        Branch 'args' and 'arg_types' must have same length")            
                
                    if branch.args_from is not None:
                        logger.warning(f"(Sample: {sample}, Tree: {tree}, Branch: {branch.write_name}) - \n \
                                          Computing branches from different trees not supported in histogramming. Ignoring the args_from arg ")
                if len(set(branchnames)) != len(branchnames):
                    logger.error("All new branch names must be unqiue")
            if len(set(treenames)) != len(treenames):
                logger.error(f"All trees for a given sample must be unique")
        if len(set(samplenames)) != len(samplenames):
                logger.error(f"All samples must be unique")
        
        return True


    def validate_settings(self):
        '''
        Make sure the general_settings map provided in config is valid
        '''
        req_settings = ['indir']
        bad_settings_msg = f"A general_settings map must be defined with at least the following keys {req_settings}"
        try:
            settings = self.module.general_settings
            # Make all settings lower case
            settings = {k.lower(): v for k, v in settings.items()}
            # Check required settings are provided 
            for req_setting in req_settings:
                settings[req_setting] # try to retrieve required setting
        except:
            logger.error(bad_settings_msg)

        # Check all settings provided are supported 
        if not all(given_setting in self.settings for given_setting in settings):
            for given_setting in self.settings:
                if given_setting not in self.settings:
                    logger.error(f'Unsupported setting {given_setting} provided. Acceptable settings are {list(self.settings)} ')
        return True

    def check_supported_settings(self):
        '''
        Check values of settings provided are supported 
        '''
        for k, v in self.settings.items():
            if k in self.supported_settings:
                if v not in self.supported_settings[k]:
                    logger.error("Unsupported value {v} for setting {k}. Supported values are  self.supported_settings[k]")

        

import uproot4 as uproot
import dask
from dask.distributed import Client
import sys, os
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from configs.thbb_boosted_sklimming_config import samples
import glob
from termcolor import cprint
from collections import defaultdict
import awkward as ak

def decorate_sample_tag(tags):
    '''
    Take a list of unique sample tags and decorate it with wildcards
    Args:
        tags:  A list of sample identifying tags
    Return:
        a list with decorated elements
    '''
    return [f'*{tag}*' for tag in tags]


def make_sample_path(locations, tags):
    '''
    Attach path pieces together to make a list of valid paths to look for sample files
    Args:
        locations: The directories to look for sample
        tags: The decorated tags to identift sample
    Return:
        paths: A list of full paths up to sample tags
    '''
    paths = [str(loc)+'/'+tag for tag in tags for loc in locations]
    dirpaths = [p+'/*'  for path in paths for p in glob.glob(path) if os.path.isdir(p)]
    fpaths = list(set([path  for path in paths for p in glob.glob(path) if not os.path.isdir(p) ]))
    paths = dirpaths+fpaths

    return paths

def main():
    tree = "sumWeights"
    samp_camp_data = defaultdict( dict) 
    for sample in samples:
        paths = make_sample_path(sample.location, decorate_sample_tag(sample.tag) )
        files = []
        for path in paths:
            files = glob.glob(path)
            
            file_tree = [f"{f}:{tree}" for f in files]
            data = uproot.concatenate(file_tree, filter_name = ["totalEventsWeighted", "AMITag", "dsid"], library = "ak")
            tow = ak.sum(data["totalEventsWeighted"])
            try:
                samp_camp_data[f"{data.dsid[0]}"][f"{data.AMITag[0]}"] += [tow]
            except KeyError as ke:
                samp_camp_data[f"{data.dsid[0]}"][f"{data.AMITag[0]}"] = [tow]

        
        #samp_camp_data[f"{data.dsid[0]}_{data.AMITag[0]}"] = [tow]
    akarr = ak.Array(samp_camp_data)
    ak.to_json(akarr, "metadata.json")



            

if __name__ == "__main__":
    main()
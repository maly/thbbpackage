# Analysis Package for tH(bb)q 

This project is a python-based, object-oriented framework to run various important elements of the tH(bb)q analysis. 
**This project is in it's very early stages**

The frameowrk so far supports Sklimming (Slimming+Skimming) and Plotting. To use the framework, you will need to always run the sklimming code first. The outputs from sklimming (in `.h5` format) are then consumed by other parts of the framwork. 

## Dependancies
The code is written and tested on `python v3.9`. You should have the following python packages installed:
- `awkward1`
- `uproot4` 
- `h5py` 
- `numpy` 
- `time` (to be removed)
- `mplhep (plotting only)`
- `matplotlib (plotting only)`

all of which are available through `pip install` commands. 

## Downloading the code
This code is only available to be downloaded from source (gitlab). You should clone the current repository and prepare your own environment to download the dependancies in. 
### coming up
In later versions of the code, a docker image will be made available. There will also be a setup bash script to get everything ready for you on the fly. 

## Sklimming 
The first part of this framework is sklimming input data. This step is responsible for reading in `.root` files and 
selecting specific branches from them (slimming) as well as filter data using selection cuts (skimming). In addition
you can use the code to create new branches that might be of interest to the analysis. 

The steering code for this step is in `scripts/sklim.py` and suports a CLI. This is a configuration file based code. To run it, you should first set up a configuration file similar to `configs/sklimming_config.py`. From the current directory, run 
```
python scripts/sklim.py -c <path_to_config>
```

### The Configuration File
The code makes use of a powerful python-module as a configuration file. This allows user to take full control over the code. For the code to run you need to have the following objects
- `samples`: A list of `Sample` objects (see below for `Sample`)
- `branches`: A `dict` from tree names in root files to `Branch` objects (see below for `Branch`)
- `general_settings`: A `dict` to set various general options in the code. The only required setting is `JobName` (rest have defaults). 

The `Sample` objects are constructed as follows: 
```
Sample(<name>,<tag>,<where>,<branches>, (optional) <selection>)
```
where 
| Syntax      | Description |
| ----------- | ----------- |
| \<name\>      | Name you want to give sample       |
| \<tag\>   | A unique identifier (or `list` of them) to find files for this sample (e.g. DSIDs)        |
| \<where\>   | A directory (or `list` of directories) to find the files in        |
| \<branches\>   | A map from tree names to `Branch` objects to be extracted from this sample files        |
| \<selection\>   | A `Selection` object |

The `Selection` object is constructed as follows:

```
Selection(<func>,<args>, (optional) <label>)
```

| Syntax      | Description |
| ----------- | ----------- |
| \<func\>   | A filtering function which returns a `bool` array (with same shape as data) to be used to filter data |
| \<args\>   | A map from tree to branch names (or other args) needed to apply the cuts function (required if `\<cuts\>=True`)|
| \<cut_label\>   | An optional string label to identify the cut -- only used for plotting, if requested in plotting code |

coming up:
- List of selection objects (with AND/OR support)
- Allowing for string selection for simple selection 

The`Branch` objects that are  are constructed as follows:
```
Branch(<out_name>,<alg>,(optional) <args>, (optional) <args_types>, (optional) <branch_type>, (optional) <args_from>, (optional) <drop>)
```
where 
| Syntax      | Description |
| ----------- | ----------- |
| \<out_name\>      | Name you want to give to branch       |
| \<alg\>   | Either name of the branch in input, or a function to compute a new branch from inputs      |
| \<args\>   | A list of branch names (from input)/`out_name`/`float`s/`int`s to be passed as args to `<alg>` in computing new branches. If non-branches are passed as args, they have to be passed at the end of the list| 
| \<args_types\>   | A list of types (`Branch`,`int`,`float`) for each arg that is passed to `<alg>` in order|
| \<branch_type\>   | A branch can be `on` (default if `<alg>` is a `str`), `new` (default if `<alg>` is a function)|
| \<args_from\>   | If branch needs to be computed from multiple trees, specify the list of trees here|
| \<drop\>   | If this branch is only being created to be used in computing other branches, set this to True to drop it in final output|

### Notworthy Comments on Configuration file

- The names of samples must be unqiue, as well as branches for a given tree

- Functions used for `Branch` `\<alg\>` and `Sample` `\<alg\>` is aware of all global objects in the config file, so you can define a global value and pass it as an argument to these algorithm -- this is demonstrated in the `get_xsec` funciton in the example configuration. This also means they are aware of other functions in the configuration, and so functions can call others safely -- this is demonstrated in `ttb_cuts`.

- Functions are executed for each combination of `sample.where` and `sample.tag` lists, if there are more than one. This means that for example if `sample.tag` is a list of DSID, the funcitons are executed for each DSID individually. This allows the user to define functions that combine some non-changing value from another tree (e.g. `dsid` from `SumWeights` tree) with branches from another tree (e.g. `nominal_Loose`). This is demonstrated in the `calc_weights_sw` function.

- Methods that may be useful to you to manipulate data have been written in `common/user_tools.py`. Currently, the available methods are:
    - `count_jagged`: For an array that has shape `arr=[[x,y], [a,b,c]]`, `count_jagged(arr, axis =0) -> 2` and `count_jagged(arr, axis =1) -> [2,3]`. This scales to as many axes as there is nesting. 

### The Output 

The sklimming code currently outputs the sklimmed data in `.h5` files, one per sample. Each file is named after the respective sample (e.g. `ttb.h5`). In the `h5` file there is one group for the sample, then `N` groups for data from `N` trees. 

Coming up:
- Support for ROOT output
- Support for Paqruet output


## Plotting 
The plotting framework uses the `.h5` output of the Sklimming code. Currently, the plotting framework is currently able to make the following plots:
- 1D-Histograms 

### Coming up
- 2D Histograms + Projections 
- Pull Plots from text files (and overlaying pulls)
- Yield Tables for various samples 

The steering code for this step is in `scripts/plot.py` and suports a CLI. This is a configuration file based code. To run it, you should first set up a configuration file similar to `configs/plotting_config.py`. From the current directory, run 
```
python scripts/plot.py -c <path_to_config>
```
### The Configuration file
For the code to run you need to have the following object in your configuration file:
- `variables`: A `dict` from tree to different `<Plot>` objects (e.g. `Hist1D`)
- `general_settings`: A `dict` to set various general options in the code. Only required setting is `inDir` (where the outputs of sklimming are stored) (the rest have defaults). 
Currently the only supported plot objects are:
- `Hist1D`
-
The `Hist1D` object is constructed as follows:
```
Hist1D(<var>, <binning>, <samples>, <weight>, (optional) <selection>, **kwargs)
```
| Syntax      | Description |
| ----------- | ----------- |
| \<var\>       | A `str` which is the name of the variable to plot as saved in `.h5` files from sklimming or as defined to be `Branch.write_name` in a `new_branches` object (see later on these) |
| \<binning\>   | A `VarBinning` or `RegBinning` object for variable or regular binning. These can be constrcted as `VarBinning([Edge1,Edge2,Edge3])` or `RegBinning(low,high,nbins`)|
| \<samples\>   | A list of `str` which are the sample names. These must be the same as the sample names in the `.h5` files from sklimming|
| \<weight\>    | This can be a number for a scaling weight, or a stored variable in the tree  for all samples `.h5` files or a new branch defined to in `Branch.write_name` inside a `new_branches` object (see later on these) |
| \<selection\>    | A `Selection` object as explained in the *Sklimming* section|
| \<kwargs\>    |These are plotting options. An unpacked `dict` (e.g. `plot_settings`) object can be passed using `**plot_settings` as well as keyword arguments (e.g. `xtitle="HELP"`).  An important option is `legend_labels` which takes a list of labels (equal length to list of samples with text to display in legend). A full list of supported settings will be written out soon. One important  |

You can also have an object called `new_branches` which is a `dict` from tree to `Branch` objects which are explained in the *Sklimming* section. These allow you to create new branches that will be added to all input files (i.e. to all samples). You can use the `Branch.write_name` in the plot objects to plot a new branch you created. 
>>>>>>> dev

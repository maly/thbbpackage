from thbbanalysis.plotting.classes import *

general_settings = {}
general_settings['inDir'] = './thbb_boosted_outputs/hists/'
general_settings['outDir'] = './thbb_boosted_outputs/plots_2/'
general_settings['dsiplayCuts'] = True




plot_settings = {
    #'colors': ['red','blue'],
    'ylabel': {'fontsize': 24},
    'xlabel': {'fontsize': 24},
    'ytitle': 'Number of Weighted Events (Normalised)',
    'legend_settings': {'fontsize': 28, 'frameon': False},
    'stacked':  False,
    'norm': True,
    'yaxis': {'ylim': (0,None)}
    #'norm_binwidth': 1,
}

plots = {}
plots['nominal_Loose'] = [Hist1DPlot('jet1_pt_RegBin', samples=['ttbar','tHjb'], **plot_settings, title = r'Test', xtitle = '$p^{jet1}_{T}$', legend_labels = [r'$t\bar{t}$','tHjb'])]


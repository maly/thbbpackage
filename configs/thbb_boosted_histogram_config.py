from thbbanalysis.common.user_tools import *
from thbbanalysis.common.branches import * 
from thbbanalysis.common.selection import * 
from thbbanalysis.histogramming.Hist import *

general_settings = {}
general_settings['OutDir'] = './TestDaskInFramework/hists/'
general_settings['inDir']  = "./TestDask/TestParquetOutput/"#'/eos/user/m/maly/thbb_boost/samples/'
general_settings['inFormat']  = 'parquet'
general_settings['dask']  = True
general_settings['view']  = False
histograms = {}


def njets_cut(jet_pt):
    return (count_jagged(jet_pt, axis=1) >=1)

histograms['ttbar'] = {}
histograms['ttbar']['nominal_Loose'] =      [ Hist1D('jet1_pt', 'jet1_pt_RegBin', RegBinning(0,250,25), weights = 'weight'),]

histograms['tHjb'] = {}
histograms['tHjb']['nominal_Loose'] =       [ Hist1D('jet1_pt', 'jet1_pt_RegBin', RegBinning(0,250,25), weights = 'weight'),]


def jet1_pt(jet_pt):
    jet_pt = mask(jet_pt, count_jagged(jet_pt, axis=1) >=1)
    return jet_pt[:,0]*1e-3

new_branches = {}
nl_branches = {'nominal_Loose': [Branch('jet1_pt', jet1_pt, args = ['jet_pt'], args_types=[Branch])]}
new_branches['ttbar'] = nl_branches
new_branches['tHjb'] = nl_branches

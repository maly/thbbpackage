import os,sys
from thbbanalysis.common.samples import *
from thbbanalysis.common.branches import *
from thbbanalysis.common.selection import *
from thbbanalysis.common.user_tools import *
import numpy as np
import json

def merge_dicts(a, b):
    c = a.copy()
    for k, v in b.items():
        c[k].extend(v)
    return c

nl = 'nominal_Loose'
general_settings = {}

general_settings['JobName'] = 'tHbbSM'
general_settings['OutDir'] = './outputs/tHbbSM/'
general_settings['SkipMissingFiles'] = True
general_settings['SkipMissingSamples'] = True
general_settings['DumpToFormat'] = 'parquet'

sample_dir = ['/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/tH_v34_minintuples_v0/mc16a_nom/', 
              '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/tH_v34_minintuples_v0/mc16d_nom/', 
              '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/tH_v34_minintuples_v0/mc16e_nom/',
              '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/tH_v34_minintuples_v1/data_nom/']

def calc_nom_weights(weight_mc,xsec_weight,weight_pileup, weight_bTagSF_DL1r_Continuous,weight_jvt,weight_forwardjvt,weight_leptonSF, runNumber):
    weight = (36207.66*(runNumber<290000.)+ 58450.1*(runNumber>=310000.) + 44307.4*((runNumber<310000.) & (runNumber>=290000.)))*weight_mc*xsec_weight*weight_pileup*weight_bTagSF_DL1r_Continuous*weight_jvt*weight_forwardjvt*weight_leptonSF
    return weight

def calc_weights(nom_weights, totalEventsWeighted):
    return nom_weights/totalEventsWeighted
nom_weight_branches = ['weight_mc','xsec_weight','weight_pileup','weight_bTagSF_DL1r_Continuous','weight_jvt','weight_forwardjvt','weight_leptonSF','runNumber']

branches = {}
branches[nl] = [
                Branch('bdt', 'BDT'),
                Branch('foam_1', lambda f: f[:,1], args=['foam'], args_types=[Branch]),
                Branch('njets', 'njets'),
                Branch('nbjets', 'nbjets'),
                Branch('taus_pt', 'tau_pt'),
                Branch('njets_CBT0', 'njets_CBT0'),
                Branch('njets_CBT123', 'njets_CBT123'),
                Branch('njets_CBT2', 'njets_CBT2'),
                Branch('njets_CBT3', 'njets_CBT3'),
                Branch('Ht', 'Ht'),
                Branch('njets_CBT5', 'njets_CBT5'),
                Branch('n_tophad_jets_CBT123_ttAll','n_tophad_jets_CBT123_ttAll'),
                Branch('lep_id', lambda l: l[:,0], args = ['leptons_ID'], args_types = [Branch]),
                Branch('njets_CBT4', 'njets_CBT4'),
                Branch('fwdjets0_pt', lambda j: mask(j, ak.num(j, axis=1)>=1)[:,0], args = ['fwdjets_pt'], args_types = [Branch] ),
                Branch('sphericity', 'sphericity'),
                Branch('nonbjets_pt0',lambda j: mask(j, ak.num(j, axis=1)>=1)[:,0], args =['nonbjets_pt'], args_types = [Branch]),
                Branch('chi2_min_ttAll','chi2_min_ttAll'),
                Branch('n_nontophad_jets_CBT123_ttAll','n_nontophad_jets_CBT123_ttAll'),
                Branch('nonbjets_eta0',lambda j: mask(j, ak.num(j, axis=1)>=1)[:,0], args =['nonbjets_eta'], args_types = [Branch]),
                Branch('n_tophad_jets_CBT4_ttAll','n_tophad_jets_CBT4_ttAll'),
                Branch('nonbjets_tagWeightBin_DL1r_Continuous','nonbjets_tagWeightBin_DL1r_Continuous'),
                Branch('nonbjets_pt1',lambda j: mask(j, ak.num(j, axis=1)>=2)[:,1], args =['nonbjets_pt'], args_types = [Branch]),
                Branch('lep0_tight', lambda l: l[:,0], args = ['leptons_PLIVtight'], args_types = [Branch]),
                
                Branch('HF_SimpleClassification', 'HF_SimpleClassification'),
                Branch('weight', calc_weights, args = ['nom_weight', 'totalEventsWeighted'], args_types=[Branch, Branch]),
                Branch('nom_weight', calc_nom_weights, args = nom_weight_branches, 
                        args_types=[Branch]*len(nom_weight_branches),drop=True),
                ]

data_branches, fakes_branches = {}, {}
data_branches[nl] = [br for br in branches[nl] if "weight" not in br.write_name and "HF_SimpleClassification" not in br.write_name]
fakes_branches[nl] = data_branches[nl]+[Branch('fakes_weight', lambda l: l[:,0], args = ['mm_weight'], args_types = [Branch]),]

def preselec(njets,nbjets,taus_pt):
    no_taus = (ak.num(taus_pt,axis=1)==0)
    preselection = (~((njets >= 5) & (nbjets >= 4)) & no_taus)
    return preselection

def XXX_is_b(hfclass):
    return (hfclass == 1)
def XXX_is_c(hfclass):
    return (hfclass == -1)
def XXX_is_light(hfclass):
    return (hfclass == 0)

def non_tt_sel(lep_tight,*presel):
    return ((preselec(*presel)) & (lep_tight ==1))

def ttbsel(lep_tight, hfclass, *presel):
    return ((preselec(*presel)) & (lep_tight == 1) & (XXX_is_b(hfclass)))
def ttcsel(lep_tight, hfclass, *presel):
    return ((preselec(*presel)) & (lep_tight == 1) & (XXX_is_c(hfclass)))
def ttlsel(lep_tight, hfclass, *presel):
    return ((preselec(*presel)) & (lep_tight == 1) & (XXX_is_light(hfclass)))

presel_args = {nl: ['njets','nbjets','taus_pt']}
ttbar_args = {nl: ['lep0_tight','HF_SimpleClassification']}
ttargs = dict(merge_dicts(ttbar_args, presel_args))
non_tt_args = {nl: ['lep0_tight']}
nontt_args = dict(merge_dicts(non_tt_args, presel_args))

presl_cut_label = [r'!($N_{jets}\geq5 && N^{b}_{jets}\geq4$)',r'$N^{PLIVTight}_{lep}=1$',r'$N^{b@70}_{jets}\geq3',r'$E^{miss}_T\geq25$ GeV',r'$N_{\tau}=0$']

samples = [
            Sample(name = "ttb", tag = ['410470_user'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(ttbsel, ttargs,presl_cut_label)),
            Sample(name = "ttc", tag = ['410470_user'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(ttcsel, ttargs,presl_cut_label)),
            Sample(name = "ttlight", tag = ['410470_user'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(ttlsel, ttargs,presl_cut_label)),   
            Sample(name = "ttH", tag = ['346343_user', '346344_user', '346345_user'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),
            Sample(name = "ttZ", tag = ['410156_user','410157_user','410218_user','410219_user','410220_user'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),
            Sample(name = "ttW", tag = ['412123_user', '410155_user'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)), 
            
            Sample(name = "singletop_Wtchannel", tag = ['410646_user', '410647_user'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),
            Sample(name = "singletop_tchan", tag = ['410658_user', '410659_user'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),
            Sample(name = "singletop_schannel", tag = ['410644_user', '410645_user'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),
           
            Sample(name = "tZq", tag = ['410560_user',], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),           
            Sample(name = "tWZ", tag = ['410408'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),                
            
            Sample(name = "Wjets", tag = ['364156', '364159', '364162', '364165', 
                                          '364170', '364173', '364176', '364179', 
                                          '364184', '364187', '364190', '364193', 
                                          '364157', '364160', '364163', '364166', 
                                          '364171', '364174', '364177', '364180', 
                                          '364185', '364188', '364191', '364194', 
                                          '364158', '364161', '364164', '364167', 
                                          '364172', '364175', '364178', '364181', 
                                          '364186', '364189', '364192', '364195', 
                                          '364168', '364169', '364182', '364183', 
                                          '364196', '364197'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),   
            Sample(name = "Zjets", tag =  ['364100', '364103', '364106', '364109', 
                                          '364114', '364117', '364120', '364123', 
                                          '364128', '364131', '364134', '364137', 
                                          '364101', '364104', '364107', '364110', 
                                          '364115', '364118', '364121',
                                          '364129', '364132', '364135', '364138', 
                                          '364102', '364105', '364108', '364111', 
                                          '364116', '364119', '364122', '364125', 
                                          '364130', '364133', '364136', '364139', 
                                          '364112', '364113', '364126', '364127', 
                                          '364140', '364141', '364124'],
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),   
            Sample(name = "VV", tag = ['364250_user', '364253_user', '364254_user', 
                                       '364255_user', '364288_user', '364289_user', 
                                       '364290_user', '363355_user', '363356_user', 
                                       '363357_user', '363358_user', '363359_user', 
                                       '363360_user', '363489_user', '363494_user'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),   
            
            Sample(name = "otherHiggs", tag =['342282', '342283', '342284', '342285'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),   
            Sample(name = "raretop", tag = ['304014', '412043'], 
                  where = sample_dir,branches = branches, 
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label)),   

            Sample(name = "Fakes", tag = ['data'], where = sample_dir, branches = fakes_branches,
                  selec=Selection(preselec, presel_args,presl_cut_label) ),
            Sample(name = "data", tag = ['data'], where = sample_dir, branches = data_branches,
                  selec=Selection(non_tt_sel, nontt_args, presl_cut_label) )  
          ]

import os,sys
from thbbanalysis.common.samples import *
from thbbanalysis.common.branches import *
from thbbanalysis.common.selection import *
from thbbanalysis.common.user_tools import *
import numpy as np
import json

general_settings = {}

general_settings['JobName'] = 'tHbb_boosted'
general_settings['OutDir'] = './outputs/ReplicateSM/SklimmingOutput/'
general_settings['SkipMissingFiles'] = True
general_settings['DumpToFormat'] = 'parquet'

sample_dir = ['/Users/moaly/Work/phd/thbbPackage/data/SingleTopv34ljXbbTagger/']

# =============== Weights ==========================
xsec_file = "/Users/moaly/Work/phd/thbbPackage/data/XSection-MC15-13TeV.data"
tow_file = "/Users/moaly/Work/phd/thbbPackage/thbbpackage/outputs/ReplicateSM/metadata.json"

def get_xsec(dsids, xsecs_file):
    with open(xsecs_file, "r") as f:
        lines = f.readlines()
    dsid = dsids[0]
    for line in lines:
        if "#" in line or not line.strip():
            continue
        line = line.split()
        if float(line[0]) != dsid: #dsid check
            continue
        xsec = float(line[1])
        kfact = float(line[2])
        break # only one dsid line per file 
    return xsec*kfact

def get_tow(dsids, AMITags, tow_file):
    
    with open(tow_file, "r") as f:
        data = json.load(f)[0]
    
    dsid = str(dsids[0])
    tag = str(AMITags[0])
    return data[dsid][tag]

nom_weight_branches = ['weight_mc','weight_pileup','weight_bTagSF_DL1r_Continuous','weight_jvt','weight_forwardjvt','weight_leptonSF','runNumber']

def calc_nom_weights(weight_mc,weight_pileup, weight_bTagSF_DL1r_Continuous,weight_jvt,weight_forwardjvt,weight_leptonSF, runNumber):
    weight = (36207.66*(runNumber<290000.)+ 58450.1*(runNumber>=310000.) + 44307.4*((runNumber<310000.) & (runNumber>=290000.)))*weight_mc*weight_pileup*weight_bTagSF_DL1r_Continuous*weight_jvt*weight_forwardjvt*weight_leptonSF
    return weight

def calc_weights(nom_weights, xsec_weight, totalEventsWeighted):
    return nom_weights*xsec_weight[0]/totalEventsWeighted[0]


# ==================================================
# Common Objects
# ===================================================
# Leptons
# ====================================================
def lepton(pt, eta, phi, e, charge, tight,id_quality, iso_pliv, truth_iff_class, multiply_id = 1):
    leptons = ak.zip({ "pt": pt,
                    "eta": eta,
                    "phi": phi,
                    "e": e ,
                    "ID": charge*multiply_id,
                    "tight": tight,
                    "ID_qualtiy": id_quality,
                    "iso_PLIV": iso_pliv,
                    "truthIFFClass": truth_iff_class,
                    "PLIVTight": iso_pliv*id_quality,
                    })    
    good_leptons = leptons[ (abs(leptons.eta) < 2.5) & (leptons.pt > 27e3)]
    return good_leptons

# ===================================================
# PFlow Jets
# ====================================================
def pflow_jet(pt, eta, phi, e, tagWeightBin_DL1r_Continuous, truthPartonLabel, truthflav, ):
    jets = ak.zip({ "pt": pt,
                    "eta": eta,
                    "phi": phi,
                    "e": e,
                    "pcbt": tagWeightBin_DL1r_Continuous,
                    "truthflav": truthflav,
                    "partonlabel": truthPartonLabel,
                  })    
    good_jets = jets[(jets.eta < 4.5) & (jets.pt > 25e3)]
    return good_jets

# Central PFlow Jets
def cenjets(pflow_jets):
    cen_jets = pflow_jets[(abs(pflow_jets.eta) <2.5) & (pflow_jets.pt > 25e3)]
    return cen_jets

# Forward PFlow Jets
def fwdjets(pflow_jets):
    fwd_jets = pflow_jets[( (abs(pflow_jets.eta) > 2.) & ( abs(pflow_jets.eta) < 4.5) ) & (pflow_jets.pt > 25e3)] 
    return fwd_jets

# Central b-jets (PCBT not done in forward region)
def cen_bjets(cenjets): 
    return cenjets[cenjets.pcbt >= 4] # at 70WP

# All Light jets 
def light_jets(pflow_jets):
    return pflow_jets[pflow_jets.pcbt <= 3] # at 70WP

# All Light jets 
def fwd_light_jets(fwdjets):
    return fwdjets[fwdjets.pcbt <= 3] # at 70WP

# Central non-bjets
def cen_nonbjets(pflow_jets):
    cenjets = pflow_jets[abs(pflow_jets.eta) < 2.]
    return cenjets[cenjets.pcbt <=3]

# ===================================================
# Track Jets
# ====================================================
def tjet(pt, eta, phi, e, tagWeightBin_DL1r_Continuous, DL1r):
    return ak.zip({ "pt": pt,
                    "eta": eta,
                    "phi": phi,
                    "e": e,
                    "pcbt": tagWeightBin_DL1r_Continuous,
                    "dl1r": DL1r,
                  })

# ===================================================
# LRJets
# ====================================================
def ljet(pt, eta, phi, e, m, truthLabel, xbb_top, xbb_qcd, xbb_higgs):
    return ak.zip({ "pt": pt,
                    "eta": eta,
                    "phi": phi,
                    "e": e,
                    "m": m,
                    "truthlabel": truthLabel,
                    "xbb_top": xbb_top,
                    "xbb_qcd": xbb_qcd,
                    "xbb_higgs": xbb_higgs
                  })


nl = 'nominal_Loose'
branches = {}
# Need a new set for data. 
branches[nl] = [
                Branch('el', lepton, args = ['el_pt', 'el_eta', 'el_phi', 'el_e', 'el_charge', 'el_isTight', 'el_Id_TightLH','el_Isol_PLImprovedTight', 'el_truthIFFClass', -11], args_types=[Branch]*9+[int]),
                Branch('mu', lepton, args = ['mu_pt', 'mu_eta', 'mu_phi', 'mu_e', 'mu_charge', 'mu_tight', 'mu_Id_Medium','mu_Isol_PLImprovedTight', 'mu_truthIFFClass', -13], args_types=[Branch]*9+[int]),
                Branch('jet', pflow_jet, args = ['jet_pt', 'jet_eta', 'jet_phi', 'jet_e', 'jet_tagWeightBin_DL1r_Continuous', 'jet_truthPartonLabel', 'jet_truthflav'], args_types=[Branch]*7),
                Branch('cenjet', cenjets, args = ['jet'], args_types=[Branch]),
                Branch('fwdjet', fwdjets, args = ['jet'], args_types=[Branch]),
                Branch('cen_bjets', cen_bjets, args = ['cenjet'], args_types=[Branch]),
                Branch('cen_nonbjets', cen_nonbjets, args = ['jet'], args_types=[Branch]),
                Branch('fwd_lightjets', fwd_light_jets, args = ['fwdjet'], args_types=[Branch]),
                Branch('light_jets', light_jets, args = ['jet'], args_types=[Branch]),
                Branch('n_cenjets', lambda x: ak.num(x, axis=1), args = ['cenjet'],  args_types=[Branch]),
                Branch('n_cenbjets', lambda x: ak.num(x, axis=1), args = ['cen_bjets'],  args_types=[Branch]),
                Branch('ntaus', lambda x: ak.num(x, axis=1), args = ['tau_pt'],  args_types=[Branch]),
                Branch('met', 'met_met'),
                Branch('nom_weight', calc_nom_weights, args = nom_weight_branches, 
                        args_types=[Branch]*len(nom_weight_branches),drop=True),
                Branch('weight', calc_weights, args = ['nom_weight', 'xsec_weight','TotalEventsWeighted'], 
                        args_from=['nominal_Loose','sumWeights','sumWeights'], 
                        args_types=[Branch, Branch, Branch]),
                ]

branches["sumWeights"] = [ 
                          Branch('xsec_weight', get_xsec, args = ["dsid",xsec_file], args_types=[Branch, str]),
                          Branch('TotalEventsWeighted', get_tow, args = ["dsid","AMITag",tow_file], args_types=[Branch,Branch, str]),]


def preselec(njets,nbjets, elecs, muons ,ntaus, met):
    nPLIV_leps = ak.num(elecs[elecs.PLIVTight == 1], axis = 1) + ak.num(muons[muons.PLIVTight == 1], axis = 1)
    preselection = (nPLIV_leps==1) & (met > 25e3) & (nbjets >= 3) & (ntaus == 0) & ~((njets >= 5) & (nbjets >= 4))
    return preselection

samples = [
            Sample(name = "tHjb", tag = ['346676'], 
                  where = sample_dir,branches = branches,
                  selec=Selection(preselec, {nl: ["n_cenjets", "n_cenbjets", "el", "mu", "ntaus", "met"]},"")),
            Sample(name = "ttbar", tag = ['410470'], 
                  where = sample_dir,branches = branches,
                  selec=Selection(preselec, {nl: ["n_cenjets", "n_cenbjets", "el", "mu", "ntaus", "met"]},"")),
            Sample(name = "sgtop_tchan", tag = ['410658','410659'], 
                  where = sample_dir,branches = branches,
                  selec=Selection(preselec, {nl: ["n_cenjets", "n_cenbjets", "el", "mu", "ntaus", "met"]},""))     
          ]


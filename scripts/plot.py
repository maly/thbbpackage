import os,sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
import thbbanalysis.plotting as plotting
from thbbanalysis.common import tools
#=========== Pythonic Imports 
from argparse import ArgumentParser, ArgumentTypeError

_CFG_HELP = 'The full path to the configuration file to process'

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('-c', '--cfg', required=True, help=_CFG_HELP)
    return parser.parse_args()

def main():
    args = get_args() 
    cfg_path = args.cfg
    cfg = plotting.config.PlotConfig(cfg_path)
    cfg.load()
    plotting.process.process_plots(cfg)
    
if __name__ == '__main__':
    main()
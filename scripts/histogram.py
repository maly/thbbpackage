
#============== System and Python Imports
import os,sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
#============ thbbanalysis Imports
from thbbanalysis.histogramming.config import HistConfig
from thbbanalysis.histogramming.process import process_histos
from thbbanalysis.common import tools
#=========== Pythonic Imports
from argparse import ArgumentParser, ArgumentTypeError
import dask
dask.config.set({'logging': {'distributed.client': 'info',
                             'distributed.worker': 'warning',
                             'distributed.nanny': 'warning',
                             'distributed.scheduler': 'warning',
                             'distributed.core': 'warning',
                             'bokeh.server.tornado': 'warning',
                             'distributed.batched': 'warning'}})
from dask.distributed import Client

_CFG_HELP = 'The full path to the configuration file to process'

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('-c', '--cfg', required=True, help=_CFG_HELP)
    return parser.parse_args()

def main():

    args = get_args() 
    cfg_path = args.cfg
    cfg = HistConfig(cfg_path)
    cfg.load()

    if cfg.settings["dask"]:    client = Client(n_workers = 4, threads_per_worker = 2)
    process_histos(cfg)
    if cfg.settings["dask"]:    client.shutdown()
    
if __name__ == '__main__':
    main()